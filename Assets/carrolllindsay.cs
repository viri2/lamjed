using Google.Play.Review;
using Google.Play.Core.Internal;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class carrolllindsay : MonoBehaviour
{
    
    private ReviewManager _reviewManager; 
      PlayReviewInfo _playReviewInfo;
    public  GameObject fakere;
     static carrolllindsay instance;

    public static carrolllindsay Instance
    {
        get
        {
            if (instance == null)
                instance = GameObject.FindObjectOfType(typeof(carrolllindsay)) as carrolllindsay;

            return instance;
        }
    }


    void Start()
    {
        DontDestroyOnLoad(getfakerev());

    }

    GameObject getfakerev()
    {
        if (fakere== null)
        {
            return GameObject.FindGameObjectWithTag("frev");

        }
        return fakere;
    }
    public static bool showratingTruevar = false;
    void showratingTrue()
    {
        _reviewManager = new ReviewManager();

        StartCoroutine(elainesaucedo());

    }
    public static int countclick = 0;

    public  void showFake()
    {
        if (bryonedwards.Instance.sheliasherwood) { return; }

            countclick++;
        if (countclick < 10)
        {
            return;
        }
        countclick = 0;

        if (dontshowagain) return;
        if (showdirect || PlayerPrefs.GetString("showdirect")=="tr")
        {
            showratingTruevar=true;
            PlayerPrefs.SetString("showdirect", "tr");
        }
        else
        {
            if (bryonedwards.Instance.fakerev)
            {
                getfakerev().SetActive(true);

            }
            else
            {
                showratingTruevar = true;
                PlayerPrefs.SetString("showdirect", "tr");
            }
        }
    }
  
    
    void Update()
    {
        if (showratingTruevar)
        {
            showratingTruevar = false;
            showratingTrue();
        }
    }

     public static bool dontshowagain = false;
    public static bool showdirect = false;

    public void click1star()
    {
        dontshowagain=true;
        getfakerev().SetActive(false);

    }
    public  void click2star()
    {
        dontshowagain = true;
        getfakerev().SetActive(false);

    }
    public  void click3star()
    {
        getfakerev().SetActive(false);
        showratingTruevar = true;
        showdirect=true;
    }
    public  void click4star()
    {
        getfakerev().SetActive(false);
        showratingTruevar = true;
        showdirect = true;

    }
    public  void click5star()
    {
        getfakerev().SetActive(false);
        showratingTruevar = true;
        showdirect = true;

    }

    public  void clickSubmit()
    {
        getfakerev().SetActive(false);
        showratingTruevar = true;

    }
    public  void clickNotNow()
    {
        getfakerev().SetActive(false);

    }
    public  void  clickClose()
    {
        getfakerev().SetActive(false);

    }

    public IEnumerator elainesaucedo()
    {
        yield return new WaitForSeconds(1f);
        Debug.Log("ReviewShow");
        var requestFlowOperation = _reviewManager.RequestReviewFlow();
        yield return requestFlowOperation;
        if (requestFlowOperation.Error != ReviewErrorCode.NoError)
        {
            yield break;
        }
        _playReviewInfo = requestFlowOperation.GetResult();

        var launchFlowOperation = _reviewManager.LaunchReviewFlow(_playReviewInfo);
        yield return launchFlowOperation;
        _playReviewInfo = null;
        if (launchFlowOperation.Error != ReviewErrorCode.NoError)
        {
            yield break;
        }
    }

}
