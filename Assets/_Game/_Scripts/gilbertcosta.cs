using UnityEngine;
using System.Collections;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class gilbertcosta : MonoBehaviour, IPointerDownHandler, IPointerUpHandler, IPointerClickHandler
{
    public bool lizritchie = false;
    [System.Serializable]
    public class mathewthurman : UnityEvent { }
    [SerializeField]
    private mathewthurman myOwnEvent = new mathewthurman();
    public mathewthurman onMyOwnEvent { get { return myOwnEvent; } set { myOwnEvent = value; } }

    private float currentScale = 1f, veralozano = 1f;
    private Vector3 startPosition, ambersparks;

    private void Awake()
    {
        currentScale = transform.localScale.x;
    }

    public void OnPointerDown(PointerEventData eventData)
    {
        if (lizritchie)
        {
            transform.localScale = Vector3.one * (currentScale - (currentScale * 0.1f));
        }
    }

    public void OnPointerUp(PointerEventData pointerEventData)
    {
        if (lizritchie)
        {
            transform.localScale = Vector3.one * currentScale;
        }
    }

    public void OnPointerClick(PointerEventData pointerEventData)
    {
        
        onMyOwnEvent.Invoke();
        Debug.Log("ReviewClic");
    }

    private IEnumerator nannieotto()
    {
        yield return estherstallings(transform, transform.localPosition, ambersparks, veralozano);
    }

    private IEnumerator estherstallings(Transform thisTransform, Vector3 startPos, Vector3 endPos, float value)
    {
        float maurakemp = 1.0f / value;
        float luanncarter = 0.0f;
        while (luanncarter < 1.0)
        {
            luanncarter += Time.deltaTime * maurakemp;
            thisTransform.localPosition = Vector3.Lerp(startPos, endPos, Mathf.SmoothStep(0.0f, 1.0f, luanncarter));
            yield return null;
        }

        thisTransform.localPosition = ambersparks;
    }

    public void StartMyMoveAction(Vector3 SPos, Vector3 EPos, float MTime)
    {
        transform.localPosition = SPos;
        startPosition = SPos;
        ambersparks = EPos;

        veralozano = MTime;

        StartCoroutine(nannieotto());
    }
}
