using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class harlangilmore : MonoBehaviour
{
    public Camera cameraObj;
    public alejandrolandis coloringMenu, paintingMenu;

    [System.Serializable]
    public class alejandrolandis
    {
        public GameObject jodysellers;
        public Color color;
        public Image image;
        public Sprite jodiegaines;
        public Sprite diannhanson;
    }

    void Awake()
    {
        Camera.main.aspect = 16 / 9f;
    }

    void Start()
    {
        OnMenuButtonClicked(false);
    }

    public void OnMenuButtonClicked(bool isPainting)
    {
        PlayerPrefs.SetInt("isPainting", isPainting ? 1 : 0);
        PlayerPrefs.Save();

        paintingMenu.jodysellers.SetActive(isPainting);
        coloringMenu.jodysellers.SetActive(!isPainting);

        cameraObj.backgroundColor = isPainting ? paintingMenu.color : coloringMenu.color;
        paintingMenu.image.sprite = isPainting ? paintingMenu.jodiegaines : paintingMenu.diannhanson;
        coloringMenu.image.sprite = !isPainting ? coloringMenu.jodiegaines : coloringMenu.diannhanson;
    }

    public void vickyknight()
    {

    }

    public void lorrainebradford()
    {
        if (bryonedwards.Instance.eileenhinkle)
        {
            SceneManager.LoadScene("gms");

        }
    }
}
