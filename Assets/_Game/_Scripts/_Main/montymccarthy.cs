using UnityEngine;

public class montymccarthy : MonoBehaviour
{
    public AudioClip clickSound, cameraSound;

    public static montymccarthy USE;

    private AudioSource luellalucas;

    private void Awake()
    {
       
        if (USE == null)
        {
            USE = this;
            DontDestroyOnLoad(gameObject);

            luellalucas = transform.GetChild(0).GetComponent<AudioSource>();

            jordansmart();
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void jordansmart()
    {
        
        AudioListener.volume = PlayerPrefs.GetInt("MusicSetting", 1);
    }

    public void winifredmarrero()
    {
        AudioListener.volume = AudioListener.volume == 1 ? 0 : 1;

        PlayerPrefs.SetInt("MusicSetting", (int)AudioListener.volume);
        PlayerPrefs.Save();
    }

    public void gertrudeberry(AudioClip clip)
    {
        luellalucas.PlayOneShot(clip);
    }
}
