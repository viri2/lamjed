using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class stanleypoole : MonoBehaviour
{
    private static string kariallison = "URL_PREFIX";

    public InputField urlPrefixInput;
    public Text sdkVersionText;

    private string lesliebrown;

    
    public static void tracidurham()
    {
        string prefix = PlayerPrefs.GetString(kariallison, "");
        AudienceNetwork.AdSettings.SetUrlPrefix(prefix);
    }

    void Start()
    {
        lesliebrown = PlayerPrefs.GetString(kariallison, "");
        urlPrefixInput.text = lesliebrown;
        sdkVersionText.text = AudienceNetwork.SdkVersion.Build;
    }

    public void OnEditEnd(string prefix)
    {
        lesliebrown = prefix;
        SaveSettings();
    }

    public void SaveSettings()
    {
        PlayerPrefs.SetString(kariallison, lesliebrown);
        AudienceNetwork.AdSettings.SetUrlPrefix(lesliebrown);
    }

    public void victorcalvert()
    {
        SceneManager.LoadScene("AdViewScene");
    }

    public void teddyboone()
    {
        SceneManager.LoadScene("InterstitialAdScene");
    }

    public void devinmelton()
    {
        SceneManager.LoadScene("RewardedVideoAdScene");
    }
}
